#!/bin/sh 
# This script will generate a new release of Zina2

OUTPUT_DIR="../zina2-releases" 
PREFIX="zina2_v"
DRUPAL_SUFFIX="_drupal"
STANDALONE_SUFFIX="_standalone"
WORDPRESS_SUFFIX="_wordpress"

if [ $# == 1 ]; then
	echo Generating Zina2 v$1
else 
	echo "Please enter the version number (i.e. gen_release.sh <version>)"
	exit
fi

OUTDIR_NAME=$PREFIX$1
OUTDIR=$OUTPUT_DIR/$OUTDIR_NAME
OUTDIR_DRUPAL=$OUTDIR/$OUTDIR_NAME$DRUPAL_SUFFIX
OUTFILE_DRUPAL="zina-drupal-v$1.tar.gz"
OUTDIR_STANDALONE=$OUTDIR/$OUTDIR_NAME$STANDALONE_SUFFIX
OUTFILE_STANDALONE="zina-standalone-v$1.tar.gz"
OUTDIR_WORDPRESS=$OUTDIR/$OUTDIR_NAME$WORDPRESS_SUFFIX
OUTFILE_WORDPRESS="zina_wordpress-v$1.tar.gz"
START_PWD=$PWD
if [ -d $OUTPUT_DIR/$1 ]; then
	echo $OUTDIR already exists, please remove it to continue
else
	mkdir $OUTDIR
	mkdir $OUTPUT_DIR/$1
	mkdir $OUTDIR_DRUPAL
	mkdir $OUTDIR_STANDALONE
	mkdir $OUTDIR_WORDPRESS
fi

# Create Standalone Installation
cp -Rf standalone $OUTDIR_STANDALONE/zina-standalone
cp -Rf zina $OUTDIR_STANDALONE/zina-standalone/
cd $OUTDIR_STANDALONE
echo Creating Standalone TAR
tar cfz ../../$1/$OUTFILE_STANDALONE ./zina-standalone
cd $START_PWD
rm -Rf $OUTDIR_STANDALONE

# Create Drupal Installation
cp -Rf drupal $OUTDIR_DRUPAL/zina-drupal
cp -Rf zina $OUTDIR_DRUPAL/zina-drupal/
cd $OUTDIR_DRUPAL
echo Creating Drupal TAR
tar cfz ../../$1/$OUTFILE_DRUPAL ./zina-drupal
cd $START_PWD
rm -Rf $OUTDIR_DRUPAL

# Create Wordpress Installation
cp -Rf wordpress $OUTDIR_WORDPRESS/zina-wordpress
cp -Rf zina $OUTDIR_WORDPRESS/zina-wordpress/
cd $OUTDIR_WORDPRESS
echo Creating Wordpress TAR
tar cfz ../../$1/$OUTFILE_WORDPRESS ./zina-wordpress
cd $START_PWD
rm -Rf $OUTDIR_WORDPRESS

rm -Rf $OUTDIR
