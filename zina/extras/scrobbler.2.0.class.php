<?php
/*
*/

class scrobbler {
	public $api_key = "913399d8ca22a8226428dfd05ad89e99";
	public $secret = "ca775cb43f099169a6deb5943d38c6c9";
	public $host = "ws.audioscrobbler.com";
	public $prefix = "ssl://";
	public $port = 443;
	public $path = "/2.0/";
	public $sk = null;
	public $now_playing;
	public $queued_tracks;  
	public $lfm_cmd = null;
	public $lfm_err = 0;
	public $lfm_err_str = "";
	public $username;
	public $password;

	public function __construct($username, $password, $sk=null, $now_playing=null) {
		if ($now_playing) {
			$this->now_playing = $now_playing;
		} 
		else {
			$this->now_playing = new stdClass();
		}
		$this->queue = array();
		$this->username = $username;
		$this->password = $password;
		if ($sk) {
			$this->sk = $sk;
		}
	}

	private function send_request($method, $post) {
		$fp = fsockopen($this->prefix.$this->host, $this->port, $errno, $errstr, $timeout = 30);
		$response = null;
		$request_uri = $this->path."?method=".$method;	
	
		if (!$fp) {
			echo "$errstr ($errno)\n";
		} 
		else {
			fputs($fp, "POST $request_uri HTTP/1.1\r\n");
			fputs($fp, "Host: $this->host\r\n");
			fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
			fputs($fp, "Content-length: ".strlen($post)."\r\n");
			fputs($fp, "Connection: close\r\n\r\n");
			fputs($fp, $post."\r\n\r\n");
		}

		while($fp && !feof($fp)) {
			$response = $response.fgets($fp, 4096);
		}

		fclose($fp);

		$response = strstr($response, '<?xml version="1.0" encoding="utf-8"?'.'>');
		if (!$response) {
			return null;
		}
		$xml_response = simplexml_load_string($response);
		if (!$xml_response) {
			return null;
		}
	
		if ($xml_response['status'] != 'ok') {
//			$this->lfm_err = $xml_response->error['code'];
//			$this->lfm_err_str = (string)$xml_response->error;
//			echo $this->lfm_cmd.": ".$this->lfm_err_str;
			return null;
		}
	
		return $xml_response;
	}

	public function now_playing($artist, $track) {
	
		$cmd_str = "track.updateNowPlaying";
		$this->lfm_cmd = $cmd_str;
			
		$sig_str = md5("api_key".$this->api_key.
			"artist".$artist.
			"method".$cmd_str.
			"sk".$this->sk.
			"track".$track.
			$this->secret);
		$poststring = "&api_key=".$this->api_key.
			"&api_sig=".$sig_str.
			"&artist=".$artist.
			"&track=".$track.
			"&sk=".$this->sk;

		$this->now_playing->artist = $artist;
		$this->now_playing->track = $track;
		$this->now_playing->timestamp = time();
		
		$response = $this->send_request($cmd_str, $poststring);
		if ($response) {
			return true;
		}
		else {
			return false;
		}
	}

	private function scrobble_int($track) {
		$cmd_str = "track.scrobble";
		$this->lfm_cmd = $cmd_str;

		$sig_str = md5("api_key".$this->api_key.
			"artist".$track->artist.
			"method".$cmd_str.
			"sk".$this->sk.
			"timestamp".$track->timestamp.
			"track".$track->track.
			$this->secret);
		$poststring = "&api_key=".$this->api_key.
			"&api_sig=".$sig_str.
			"&artist=".$track->artist.
			"&track=".$track->track.
			"&sk=".$this->sk.
			"&timestamp=".$track->timestamp;
		
		$response = $this->send_request($cmd_str, $poststring);
		if ($response) { 
			return true;
		}
		else {
			return false;
		}
	}

	public function scrobble() {
		$fl = false;
		// Attempt to send queued tracks
		if (count($this->queued_tracks)) {
 			foreach ($this->queued_tracks as $trackKey =>$track) {
				// Scrobble the queued track
				if ($this->scrobble_int($track)) {
					unset($this->queued_tracks[$trackKey]);
				}
				else {
					return false;
				}
			}
			$this->queued_tracks = array();
			$fl = true;
		}		

		// Try to scrobble
		if ($this->scrobble_int($this->now_playing)) {
			return true; 
		}
		else {
			// Push the track onto the queue
			array_push($this->queued_tracks, $this->now_playing);
			return false;
		}
	}

	public function do_auth() {
		$cmd_str = "auth.getMobileSession";
		$this->lfm_cmd = $cmd_str;

		$sig_str = md5("api_key".$this->api_key.
			"method".$cmd_str.
			"password".$this->password.
			"username".$this->username.
			$this->secret);

		$poststring = "&api_key=".$this->api_key.
			"&api_sig=".$sig_str.
			"&username=".$this->username.
			"&password=".$this->password;

		$response = $this->send_request($cmd_str, $poststring);
		
		if ($response) {
			$this->sk = (string)$response->session[0]->key;
			return true;
		}
		else {
			return false;
		}
	}

} // end audioscrobbler class

/*	$scrobbler = new scrobbler();
	$scrobbler->do_auth("helmespc", "ms@fr0Guns0u");
	$scrobbler->now_playing("Smashing Pumpkins", "Tonight, Tonight");
	sleep(60);
	$scrobbler->scrobble();*/
?>
